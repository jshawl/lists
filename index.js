var express = require("express")
var app = express()
var bodyParser = require("body-parser")
app.use(bodyParser.urlencoded())
app.use(express.static("public"))
app.set('view engine','hbs')
var methodOverride = require('method-override')
app.use(methodOverride('_method'))

var db = require("./db/models")
var Account = db.Account
var List = db.List
var Item = db.Item

app.get("/", (_,res) =>{
  res.render("index")
})
app.get("/api/accounts", (req, res) => {
  Account.findAll({include:[{model:List, include: [Item]}]}).then((doc) => {
    res.send(doc)
  })
})
app.post("/accounts", (req, res) => {
  Account.create({}).then((doc) => {
    res.redirect("/accounts/" + doc.id)
  })
})
app.get("/accounts/:id", (req, res) => {
  Account.findById(req.params.id, {include: {model: List, include: {model: Item}}}).then((doc) => {
    res.render("lists/index",doc.dataValues)
  })
})
app.get("/api/accounts/:id/lists", (req, res) => {
  List.findAll({where:{accountId: req.params.id},include: {model: Item}}).then((doc) => {
    res.send(doc)
  })
})
app.get("/accounts/:account_id/lists/:id", (req, res) => {
  List.findById(req.params.id,{include: {model: Item}}).then((doc) => {
    console.log(doc.dataValues)
    res.render("lists/show", doc.dataValues)
  })
})
app.delete("/accounts/:account_id/lists/:id", (req, res) => {
  List.destroy({where: {id:req.params.id}}).then(doc => {
      res.redirect("/accounts/" + req.params.account_id)
  })
})

app.delete("/api/accounts/:account_id/lists/:list_id/items/:id", (req, res) => {
  Item.destroy({where: {id: req.params.id}}).then(_=>{
    res.send("")
  })
})

app.patch("/api/accounts/:account_id/lists/:id", (req, res) => {
  List.update({name: req.body.title},{where: {id: req.params.id}}).then(doc => {
    res.send(doc)
  })
})

app.patch("/api/accounts/:account_id/lists/:list_id/items/:id", (req, res) => {
  Item.update({name: req.body.name},{where: {id: req.params.id}}).then(doc => {
    res.send(doc)
  })
})

app.post("/accounts/:account_id/lists", (req, res) => {
  List.create({name: 'New List', accountId: req.params.account_id}).then(d => {
    res.redirect("/accounts/" + req.params.account_id + "/lists/" + d.id)
  })
})

app.post("/api/accounts/:account_id/lists/:list_id/items", (req, res) => {
  Item.create({name: req.body.name, listId: req.params.list_id}).then(d => {
    res.send(d)
  })
})
app.listen(3000)
