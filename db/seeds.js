var db = require("./models")
var Account = db.Account
var List = db.List
var Item = db.Item

db.db.sync().then(_=>{
  Account.create({
    lists: [
      {name: 'First List', items:[
        {name: 'First item'}
      ]},
      {name: 'Second List'}
    ]
  }, {include: [{model:List,include: [Item]}]}).then(acct=>{
    process.exit
  })
})
