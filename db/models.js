var db = require('./connection')
var S = require('sequelize')

var Account = db.define('account',{})
var List = db.define('list', {name: S.STRING})
var Item = db.define('item', {name: S.STRING})

Account.hasMany(List)
List.belongsTo(Account)
List.hasMany(Item)
Item.belongsTo(List)

module.exports = {
  db,
  List,
  Item,
  Account
}
